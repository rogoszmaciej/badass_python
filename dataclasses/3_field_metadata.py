from dataclasses import dataclass, field
from datetime import datetime
from decimal import Decimal


@dataclass
class Payment:
    amount: Decimal
    datetime: datetime
    user_email: str = field(repr=False, init=False)
    origin: str = field(default="API")


payment = Payment(
    amount=Decimal("1.00"),
    datetime=datetime.strptime("2022-11-06", "%Y-%M-%d"),
)
payment.user_email = "john@doe.com"


print(f"Repr: {payment.__repr__()}")
print(f"Dict: {payment.__dict__}")
