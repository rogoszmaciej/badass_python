from decimal import Decimal

from dataclasses import dataclass


@dataclass(slots=True)
class Payment:
    amount: Decimal
    origin: str


payment = Payment(amount=Decimal("1.00"), origin="API")
