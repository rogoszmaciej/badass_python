from decimal import Decimal

from dataclasses import (
    asdict,
    dataclass,
)


@dataclass
class Payment:
    amount: Decimal
    origin: str = "API"


payment = Payment(amount=Decimal("1.00"))
print(asdict(payment))
