from dataclasses import dataclass, field
from datetime import datetime
from decimal import Decimal


@dataclass
class Payment:
    amount: Decimal
    datetime: datetime
    user_email: str = field(repr=False, init=False)
    origin: str = field(default="API")

    def __post_init__(self):
        if isinstance(self.datetime, str):
            try:
                self.datetime = (datetime.strptime(self.datetime, "%Y-%M-%d"),)
            except ValueError as exc:
                raise ValueError("Conversion to datetime failed") from exc


payment = Payment(
    amount=Decimal("1.00"),
    datetime="2022-10-06",
)


print(f"Repr: {payment.__repr__()}")
print(f"Dict: {payment.__dict__}")
