from dataclasses import dataclass
from decimal import Decimal


@dataclass
class Payment:
    amount: Decimal
    status: int
    user_email: str

    def __init__(self, amount: Decimal, status: int, user_email: str) -> None:
        self.amount = amount
        self.status = status
        self.user_email = user_email

    def __repr__(self):
        return f"Payment(amount={self.amount!r}, status={self.status!r}, user_email={self.user_email!r})"

    def __eq__(self, other):
        if other.__class__ is self.__class__:
            return (self.amount, self.status, self.user_email) == (
                other.amount,
                other.amount,
                other.user_email,
            )
        return NotImplemented

    def __ne__(self, other):
        if other.__class__ is self.__class__:
            return (self.amount, self.status, self.user_email) != (
                other.amount,
                other.amount,
                other.user_email,
            )
        return NotImplemented

    def __lt__(self, other):
        if other.__class__ is self.__class__:
            return (self.amount, self.status, self.user_email) < (
                other.amount,
                other.amount,
                other.user_email,
            )
        return NotImplemented

    def __le__(self, other):
        if other.__class__ is self.__class__:
            return (self.amount, self.status, self.user_email) <= (
                other.amount,
                other.amount,
                other.user_email,
            )
        return NotImplemented

    def __gt__(self, other):
        if other.__class__ is self.__class__:
            return (self.amount, self.status, self.user_email) > (
                other.amount,
                other.amount,
                other.user_email,
            )
        return NotImplemented

    def __ge__(self, other):
        if other.__class__ is self.__class__:
            return (self.amount, self.status, self.user_email) >= (
                other.amount,
                other.amount,
                other.user_email,
            )
        return NotImplemented
