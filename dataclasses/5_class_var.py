from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal
from typing import ClassVar


@dataclass
class Payment:
    amount: Decimal
    datetime: datetime
    origin: str = "API"
    class_var: ClassVar[str] = "Class variable"


payment = Payment(
    amount=Decimal("1.00"),
    datetime=datetime.strptime("2022-11-06", "%Y-%M-%d"),
)


print(payment.__repr__())
print(Payment.class_var)
payment.class_var = "def"
print(payment.class_var)
print(Payment.class_var)
