from dataclasses import dataclass, field
from datetime import datetime
from decimal import Decimal


@dataclass
class Payment:
    amount: Decimal
    datetime: datetime
    user_email: str = field(repr=False, init=False)
    origin: str = field(default="API")

    def __post_init__(self):
        if self.datetime < datetime.now():
            raise ValueError("Datetime can't be in the past!")


payment = Payment(
    amount=Decimal("1.00"),
    datetime=datetime.strptime("2022-10-06", "%Y-%M-%d"),
)


print(f"Repr: {payment.__repr__()}")
print(f"Dict: {payment.__dict__}")
