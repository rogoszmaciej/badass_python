from decimal import Decimal

from dataclasses import (
    asdict,
    dataclass,
    replace,
)


@dataclass(frozen=True)
class Payment:
    amount: Decimal
    origin: str = "API"


payment = Payment(amount=Decimal("1.00"))
payment = replace(payment, origin="New origin")
print(asdict(payment))
