from dataclasses import dataclass, field
from datetime import datetime
from decimal import Decimal


@dataclass
class Payment:
    amount: Decimal
    datetime: datetime
    origin: str = field(default="API")
    installments: list = field(default_factory=list)


payment = Payment(
    amount=Decimal("1.00"),
    datetime=datetime.strptime("2022-11-06", "%Y-%M-%d"),
)


print(payment.__repr__())
