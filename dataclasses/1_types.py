from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal


@dataclass
class Payment:
    amount: Decimal
    datetime: datetime
    origin: str


payment = Payment(
    amount=Decimal("1.00"),
    datetime=datetime.strptime("2022-11-06", "%Y-%M-%d"),
    origin="API",
)
print(payment.__repr__())
